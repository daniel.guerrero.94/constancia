<?php
require "fpdf/fpdf.php";
require "HTML_ENTITIES_DECODE.php";
require "PDF.php";

$inst_html_entities = new HTML_ENTITIES_DECODE();
$apellidos          = $inst_html_entities->text_to_pdf_decode($_GET['apellidos']);
$nombres            = $inst_html_entities->text_to_pdf_decode($_GET['nombres']);
$tipo_documento     = $inst_html_entities->text_to_pdf_decode($_GET['tipo_doc']);
$numero_documento   = $_GET['doc'];
$clave_beneficiario = $_GET['clave_benef'];
$fecha_ins          = $_GET['fecha_ins'];
$provincia          = $inst_html_entities->text_to_pdf_decode($_GET['provincia']);
$cuie               = $inst_html_entities->text_to_pdf_decode($_GET['cuie']);
$nombre_efector = "";

if ( ! ($conexion = pg_connect("host=192.6.0.66 dbname=sirge3 port=5432 user=postgres password=BernardoCafe008")))
	{
		/*Si la conexion no es exitosa se mostrara el siguiente mensaje y salimos*/
		echo "No pudo conectarse al servidor";
		exit();
	}
	else
	{

		if($cuie != "EN TRAMITE"){

			pg_set_client_encoding($conexion, "UTF-8");

			$sql = "SELECT nombre
						 FROM efectores.efectores e
						 WHERE cuie = '".$cuie."' ";					 					 

			$query = pg_query($conexion, $sql);

			if ( ! $query)
			{
				echo "Error en la query 1.\n";
				echo pg_last_error();
				exit;
			}

			if(pg_num_rows($query) > 0){
				$array_resultados = pg_fetch_all($query);
				$nombre_efector = $inst_html_entities->text_to_pdf_decode($array_resultados[0]['nombre']);		
			}
		}
		else{
			$nombre_efector = "EN TRAMITE";
		}
		
	}

		



/*$area = Areas::getNombre($datosUsuario['area']);
$area = $inst_html_entities->text_to_pdf_decode($area);
unset($_GET['id_usuario']);*/
$pdf = new PDF('P', 'mm', 'A4');
//Títulos de las columnas
$columnas = array('Apellidos', '', '', '');
$pdf->AliasNbPages();
//Primera página
$pdf->AddPage();

$pdf->SetDrawColor(153, 204, 255);
$pdf->SetLineWidth(0.7);
$pdf->Rect(10, 14, 190, 96, 'D');

$y = 45;
$pdf->SetY($y);
$pdf->SetX(50);
$pdf->SetDrawColor(225, 225, 225);
$pdf->SetLineWidth(0.3);
$pdf->Rect(20, 48, 170, 59, 'D');
$pdf->SetTextColor(90, 90, 90);
$pdf->SetFont('Arial', '', 11);
$y += 7;
$pdf->SetY($y);
$pdf->SetX(21);
$pdf->Cell(0, 0, "Apellidos: ", 0, 1);
$pdf->SetX(67);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $apellidos, 0, 1);
$ylinea = 57;
$pdf->Line(20, $ylinea, 190, $ylinea);
$y += 10;
$ylinea += 10;
$pdf->SetY($y);
$pdf->SetX(21);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Nombres: ", 0, 1);
$pdf->SetX(67);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $nombres, 0, 1);
$pdf->Line(20, $ylinea, 190, $ylinea);
$y += 10;
$ylinea += 10;
$pdf->SetY($y);
$pdf->SetX(21);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Tipo de Documento: ", 0, 1);
$pdf->SetX(67);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $tipo_documento, 0, 1);

$pdf->SetX(106);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Numero de Documento: ", 0, 1);
$pdf->SetX(157);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $numero_documento, 0, 1);

$pdf->Line(20, $ylinea, 190, $ylinea);

$y += 10;
$ylinea += 10;
$pdf->SetY($y);
$pdf->SetX(21);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Clave de Beneficiario: ", 0, 1);
$pdf->SetX(67);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $clave_beneficiario, 0, 1);

$pdf->SetX(106);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Fecha de Inscripcion: ", 0, 1);
$pdf->SetX(157);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $fecha_ins, 0, 1);

$pdf->Line(20, $ylinea, 190, $ylinea);

$y += 10;
$ylinea += 10;
$pdf->SetY($y);
$pdf->SetX(21);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Establecimiento: ", 0, 1);
$pdf->SetX(67);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $nombre_efector, 0, 1);

$pdf->Line(20, $ylinea, 190, $ylinea);

$y += 10;
$ylinea += 10;
$pdf->SetY($y);
$pdf->SetX(21);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Provincia: ", 0, 1);
$pdf->SetX(67);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, $provincia, 0, 1);

/*$pdf->SetX(106);
$pdf->SetFont('Arial', '', 11);
$pdf->SetTextColor(90, 90, 90);
$pdf->Cell(0, 0, "Establecimiento: ", 0, 1);
$pdf->SetX(136);
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetTextColor(55, 55, 55);
$pdf->Cell(0, 0, substr($nombre_efector, 0,20), 0, 1);*/
if($clave_beneficiario == "EN TRAMITE"){
	$nrobenef = $numero_documento;
}
else{
	$nrobenef = $clave_beneficiario;	
}


$pdf->Output();
$pdf->Output("impresiones/constancia_benef_" . $nrobenef . ".pdf", 'F');

?>