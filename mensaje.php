<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="http://programasumar.com.ar/favicon.ico">
<title>Programa SUMAR - Ministerio de Salud de la Naci&oacute;n</title>
<style type="text/css">
<!--
body {
	background-image: url(imagenes/fondo.jpg);
	background-repeat: repeat-x;
	background-attachment:fixed;
	background-color: #E7E7E7;
}
.Fecha {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #999999;
}
.Area {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #009ADF;
}
.Aviso {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #990000;
}
a:link {
	color: #FFFFFF;
	text-decoration: none;
}
a:visited {
	color: #FFFFFF;
	text-decoration: none;
}
a:hover {
	text-decoration: none;
	color: #333333;
}
a:active {
	text-decoration: none;
	color: #FFFFFF;
}
.Calendario{
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #333333;
}
.Calendario2{
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	color: #333333;
}
.GrisClaro{
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #999999;
}
.Grande {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #666666;
}
.Grande2 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #009ADF;
	font-weight: bold;
}
.Grande3 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #666666;
	font-weight: bold;
}
.Cargo {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #009ADF;
	font-weight: bold;
}
.Cargo2 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	font-weight: bold;
}
.Texto {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #333333;
}
.TituloBlanco {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #FFFFFF;
}
.TextoBlanco {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
	color: #FFFFFF;
}
.Estilo1 {font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 16px; color: #009ADF; font-weight: bold; }
-->
</style>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
</head>

<body>
<table width="100" border="0" align="center" cellpadding="0" cellspacing="2">
  <tr>
    <td height="528" align="center"><table width="1000" height="521" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="8">&nbsp;</td>
        <td width="800" valign="bottom"><img src="imagenes/somb.png" width="100%" height="11" /></td>
        <td width="8">&nbsp;</td>
      </tr>
      <tr>
        <td rowspan="3" align="right" valign="top"><img src="imagenes/somb2.png" width="8" height="450" /></td>
        <td background="imagenes/gris.jpg" bgcolor="#FFFFFF"><table width="99%" border="0" cellspacing="20" cellpadding="0">
            <tr>
              <td width="71" align="center"><a href="http://www.msal.gov.ar/sumar/"><img src="imagenes/logo_sumar.png" alt="Programa SUMAR" width="71" height="71" border="0" /></a></td>
              <td width="555" align="center"><div align="left"><span class="Estilo1">CONSTANCIA DE INSCRIPCI&Oacute;N</span></div></td>
              <td width="87" align="center"><a href="http://www.plannacer.msal.gov.ar/"><img src="imagenes/logo_plan-nacer.png" alt="Plan Nacer" width="87" height="55" border="0" /></a></td>
              <td width="160" align="right"><a href="http://www.msal.gov.ar/"><img src="imagenes/logo_msal.png" alt="Ministerio de Salud de la Nación" width="160" height="55" border="0" /></a></td>
              </tr>
        </table></td>
        <td rowspan="3" align="left" valign="top"><img src="imagenes/somb3.png" width="8" height="450" /></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1%" background="imagenes/barra.gif" bgcolor="#4396DE"><img src="imagenes/barra.gif" width="2" height="33" /></td>
            <td width="99%" align="right" background="imagenes/barra.gif" bgcolor="#4396DE">&nbsp;</td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td height="343" valign="top" bgcolor="#FFFFFF"><table width="983" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" valign="top"><p>&nbsp;</p>
            <table width="31%" border="0" cellspacing="20">
                <tr>
                  <td align="center" nowrap="nowrap" class="cumple2"><img src="imagenes/atencion.gif" width="67" height="67" /></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Grande4"><div align="center" class="Estilo1">No se encontró el registro.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Puede deberse a que realizó la inscripción recientemente<br />o que no se encuentra inscripto.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Para inscribise en el Programa SUMAR deber&aacute;<br />
                    concurrir al centro de salud u hospital p&uacute;blico<br />
                    m&aacute;s cercano a su domicilio con su DNI. </div></td>
                </tr>
              </table>
              <p>&nbsp;</p>
              <table width="31%" border="0" cellspacing="20">
                <tr>
                  <td align="center" nowrap="nowrap" class="cumple2"><img src="imagenes/atencion.gif" width="67" height="67" /></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Grande4"><div align="center" class="Estilo1">La inscripción no se encuentra activa.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Puede deberse a que posee obra social o prepaga.<br />O no se encuentra dentro de la poblaci&oacute;n objetivo del Programa SUMAR.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Para mas informaci&oacute;n puede comunicarse de manera<br />
                    gratuita llamando desde un tel&eacute;fono fijo al 0800.222.7100.</div></td>
                </tr>
              </table>
              <p>&nbsp;</p></td>
            </tr>
        </table></td>
        </tr>
      <tr>
        <td colspan="3" align="right" valign="top"><img src="imagenes/somb4.png" width="100%" height="11" /></td>
        </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>
