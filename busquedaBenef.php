<?php

$doc = $_POST['doc'];

$row = null;

if(is_numeric($doc)){

	if ( ! ($conexion = pg_connect("host=192.6.0.66 dbname=sirge3 port=5432 user=postgres password=BernardoCafe008")))
	{
		/*Si la conexion no es exitosa se mostrara el siguiente mensaje y salimos*/
		echo "No pudo conectarse al servidor";
		exit();
	}
	else
	{

		pg_set_client_encoding($conexion, "UTF-8");

		$sql = "SELECT b.clave_beneficiario, b.apellido, b.nombre, b.tipo_documento, b.numero_documento, b.fecha_inscripcion, b.id_provincia_alta, bp.periodo, bp.activo, bp.efector_asignado as cuie, bb.motivo, bb.mensaje
					 FROM beneficiarios.beneficiarios b
					 INNER JOIN beneficiarios.periodos bp ON b.clave_beneficiario = bp.clave_beneficiario
					 LEFT JOIN beneficiarios.bajas bb ON b.clave_beneficiario = bb.clave_beneficiario AND bp.periodo = bb.periodo
					 where numero_documento = '$doc'
					 AND clase_documento = 'P'
					 AND bp.periodo = (SELECT max(bp.periodo) FROM beneficiarios.beneficiarios b
									 INNER JOIN beneficiarios.periodos bp ON b.clave_beneficiario = bp.clave_beneficiario
									 WHERE numero_documento = '$doc'
									 AND clase_documento = 'P')";
		

		$query = pg_query($conexion, $sql);

		if ( ! $query)
		{
			echo "Error en la query 1.\n";
			echo pg_last_error();
			exit;
		}

		$array_resultados = pg_fetch_all($query);		
		

		if (pg_num_rows($query) >= 1)
		{

			$termino = false;
			$i = 0;

			while ($i < count($array_resultados) && $termino == false)
			{

				if (substr($array_resultados[$i]['clave_beneficiario'], 0, 2) != "00")
				{
					$row = $array_resultados[$i];

					$id_provincia = $row['id_provincia_alta'];
					
					$sql = "SELECT i.descripcion FROM geo.provincias i WHERE i.id_provincia = '$id_provincia' ";

					$query = pg_query($conexion, $sql);

					if ( ! $query)
					{
						echo "Error en la query 1.\n";
						echo pg_last_error();
						exit;
					}

					$resultado = pg_fetch_assoc($query);

					$row['provincia'] = strtoupper($resultado['descripcion']);

					if ($row['provincia'] == "CABA")
					{
						$row['provincia'] = "CAPITAL FEDERAL";
					}					

					$row['provincia'] = ucwords($row['provincia']);

					if ($row['activo'] == 'S')
					{
						$termino = true;
					}
				}
				$i++;
			}
			if ($row['provincia'] == "")
			{
				unset($row['provincia']);
			}			
		}
		else
		{	
			$sql = " SELECT tipodocumento, apellido, nombre, provincia, created_at  
							FROM siisa.renaper 
							WHERE nrodocumento = '$doc' ";

			$query = pg_query($conexion, $sql);
			$resultado = pg_fetch_assoc($query);
			
			if (pg_num_rows($query) >= 1){
				$row['tipo_documento'] = convertirEnTexto($resultado['tipodocumento']);
				$row['numero_documento'] = convertirEnTexto($doc);
				$row['apellido'] = convertirEnTexto($resultado['apellido']);
				$row['nombre'] = convertirEnTexto($resultado['nombre']);				
				$row['provincia'] = convertirEnTexto($resultado['provincia']);				
				$row['fecha_inscripcion'] = date('d-m-Y', strtotime($resultado['created_at']));			
				$row['cuie'] = "EN TRAMITE";
				$row['clave_beneficiario'] = "EN TRAMITE";
				$row['activo'] = "S";
			}
			else
			{				
				$json = file_get_contents("http://localhost/sirge3/public/consulta-siisa-renaper/" . $doc);
				$data = json_decode($json);

				$row = $data;

				if(isset($data->nombre)){

					$row->tipo_documento = convertirEnTexto($data->tipoDocumento);
					$row->numero_documento = convertirEnTexto($doc);
					$row->apellido = convertirEnTexto($data->apellido);
					$row->nombre = convertirEnTexto($data->nombre);
					//$row->sexo = $data->sexo;
					//$row->fechaNacimiento = $data->fechaNacimiento;
					$row->provincia = convertirEnTexto($data->provincia);
					$row->fecha_inscripcion = date('Y-m-d');				
					$row->cuie = "EN TRAMITE";
					$row->clave_beneficiario = "EN TRAMITE";
					$row->activo = "S";

					if ($row->provincia == "CABA")
					{
						$row->provincia = "CAPITAL FEDERAL";
					}					

					$row->provincia = ucwords($row->provincia);

					pg_set_client_encoding($conexion, "UTF-8");

					$sql = "SELECT id FROM siisa.renaper WHERE id = '".convertirEnTexto($data->id)."'";

					$query = pg_query($conexion, $sql);				
					$resultado = pg_fetch_assoc($query);				

					if(!$resultado){
						$sql = " INSERT INTO siisa.renaper (id,codigosisa,identificadorenaper,padronsisa,tipodocumento,nrodocumento,apellido,nombre,sexo,fechanacimiento,estadocivil,provincia,departamento,localidad,domicilio,pisodpto,codigopostal,paisnacimiento,provincianacimiento,localidadnacimiento,nacionalidad,fallecido,fechafallecido,donante,created_at,base) SELECT 				
						'".convertirEnTexto($data->id)."',
						'".convertirEnTexto($data->codigoSISA)."',
						'".convertirEnTexto($data->identificadoRenaper)."',
						'".convertirEnTexto($data->PadronSISA)."',
						'".convertirEnTexto($data->tipoDocumento)."',
						'".convertirEnTexto($data->nroDocumento)."',
						'".convertirEnTexto($data->apellido)."',
						'".convertirEnTexto($data->nombre)."',
						'".convertirEnTexto($data->sexo)."',
						'".convertirEnTexto($data->fechaNacimiento)."',
						'".convertirEnTexto($data->estadoCivil)."',
						'".convertirEnTexto($data->provincia)."',
						'".convertirEnTexto($data->departamento)."',
						'".convertirEnTexto($data->localidad)."',
						'".convertirEnTexto($data->domicilio)."',
						'".convertirEnTexto($data->pisoDpto)."',
						'".convertirEnTexto($data->codigoPostal)."',
						'".convertirEnTexto($data->paisNacimiento)."',
						'".convertirEnTexto($data->provinciaNacimiento)."',
						'".convertirEnTexto($data->localidadNacimiento)."',
						'".convertirEnTexto($data->nacionalidad)."',
						'".convertirEnTexto($data->fallecido)."',
						'".convertirEnTexto($data->fechaFallecido)."',
						'".convertirEnTexto($data->donante)."',
						'now()','constancia'    
						";

						$query = pg_query($conexion, $sql);					
					}						
				}
				else
				{				
					$row = "";
				}						
			}		
		}
		

		echo json_encode($row);		
	}
}
else
{
		$row = "";				
		echo json_encode($row);
}



function convertirEnTexto($valor){
	if(gettype($valor) == "object"){
		if($valor->{'0'} == ' ' || $valor->{'0'} == ''){
			return null;
		}
		else{
			return $valor->{'0'};	
		}		
	}
	else{
		if($valor == 'NULL'){
			return null;
		}
		else{
			return $valor;
		}		
	}
}

?>