<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>

<link href="css/core.css" rel="stylesheet" media="screen" type="text/css" />
<link href="css/core.css" rel="stylesheet" media="print" type="text/css" />

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.66.0-2013.10.09/jquery.blockUI.min.js"></script>

<script src="js/jquery.PrintArea.js_4.js"></script>
<script src="js/core.js"></script>


<link rel="shortcut icon" href="http://programasumar.com.ar/favicon.ico">
<title>Programa SUMAR - Ministerio de Salud de la Naci&oacute;n</title>
<style type="text/css">
body {
	background-image: url(imagenes/fondo.jpg);
	background-repeat: repeat-x;
	background-attachment:fixed;
	background-color: #E7E7E7;
}
.Fecha {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #999999;
}
.Area {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #009ADF;
}
.Aviso {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #990000;
}
a:link {
	color: #FFFFFF;
	text-decoration: none;
}
a:visited {
	color: #FFFFFF;
	text-decoration: none;
}
a:hover {
	text-decoration: none;
	color: #333333;
}
a:active {
	text-decoration: none;
	color: #FFFFFF;
}
.Calendario{
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #333333;
}
.Calendario2{
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight:bold;
	color: #333333;
}
.GrisClaro{
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #999999;
}
.Grande {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #666666;
}
.Grande2 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #009ADF;
	font-weight: bold;
}
.Grande3 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #666666;
	font-weight: bold;
}
.Cargo {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #009ADF;
	font-weight: bold;
}
.Cargo2 {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	font-weight: bold;
}
.Texto {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #333333;
}
.TituloBlanco {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #FFFFFF;
}
.TextoBlanco {
	font-family: Geneva, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: bold;
	color: #FFFFFF;
}
#datos_a_imprimir{
  margin-left: 340px;
  margin-top: 130px;
  border-color: lightblue;
  border-style: solid;
  width: 750px;
}

#tabla_datos_a_imprimir
{
  position: relative;
  text-align: left;
  border-style: solid;
  width: 700px;
  margin: 25px;
  border: 1px solid;
  border-color: #B0B0B0;
  border-collapse: collapse;
}

#tabla_datos_a_imprimir td
{
  border-top: 1px solid;
  border-color: #B0B0B0;
  border-collapse: collapse;
  padding: 5px;
  width: 25%;
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #333333;
}

#titulos{
  position: relative;
  width: 700px;
  margin: 25px;
}

.respuesta{
  font-weight: bold;
}

.Estilo1 {font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 16px; color: #009ADF; font-weight: bold; }

</style>
</head>

<body>
<script type="text/javascript">
  $(document).ready(function(event) {
      $("#tabla_beneficiario").show();
      console.log("<?php echo $_GET['benef_documento']?>");
      var documento = "<?php echo $_GET['benef_documento']?>";

      $.blockUI({
        message: '<h2 style="text-align:center;">Cargando...</h2>',
        css: {
            border: '',
            backgroundColor: '',
            color: '#FFFFFF',
            fontWeight: 900
        }
    });

      var datos = "doc="+documento;


      $.ajax({
        url: 'busquedaBenef.php',
        type: 'GET',
        dataType: 'json',
        data: datos,
        success: function(data){
           console.log(data);

           if(data == null || data == []){
            $("#mensaje_no_encontrado").show();
           }
           else if(data.activo == "S"){
            $("#imprimir").show();
            $("#tabla_beneficiario").show();
           }
           else if(data.activo == "N"){
            $("#mensaje_no_activo").show();
           }

           $("#apellidos").text(data.apellido);
           $("#nombres").text(data.nombre);
           $("#tipo_doc").text(data.tipo_documento);
           $("#doc").text(data.numero_documento);
           $("#clave_benef").text(data.clave_beneficiario);
           $("#fecha_ins").text(data.fecha_inscripcion);
           $("#provincia").text(data.provincia);
           $("#cuie").text(data.cuie);

           $("#apellidos_imp").text(data.apellido);
           $("#nombres_imp").text(data.nombre);
           $("#tipo_doc_imp").text(data.tipo_documento);
           $("#doc_imp").text(data.numero_documento);
           $("#clave_benef_imp").text(data.clave_beneficiario);
           $("#fecha_ins_imp").text(data.fecha_inscripcion);
           $("#provincia_imp").text(data.provincia);
           $("#cuie_imp").text(data.cuie);
        }
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
         $.unblockUI();
        console.log("complete");
      });

  });
</script>
<div id="datos_a_imprimir">
<table id="titulos">
  <tr>
     <td width="555" align="left"><span class="Estilo1">CONSTANCIA DE INSCRIPCI&Oacute;N<BR /> PARA EL BENEFICIARIO</span></td>
     <td></td>
     <td width="300" align="right" class="Estilo1"><img src="imagenes/logos.gif" width="300" height="71" /></td>
  </tr>
  </table>
<table id="tabla_datos_a_imprimir">

  <tr>
    <td colspan="1">Apellidos:</td>
    <td colspan="3" class="respuesta" id="apellidos"></td>
  </tr>
  <tr>
    <td colspan="1">Nombres:</td>
    <td colspan="3" class="respuesta" id="nombres"></td>
  </tr>
  <tr>
    <td>Tipo de Documento:</td>
    <td class="respuesta" id="tipo_doc"></td>
    <td>Numero de documento:</td>
    <td class="respuesta" id="doc"></td>
  </tr>
  <tr>
    <td>Clave de Beneficiario:</td>
    <td class="respuesta" id="clave_benef"></td>
    <td>Fecha de Inscripción:</td>
    <td class="respuesta" id="fecha_ins"></td>
  </tr>
  <tr>
    <td>Provincia:</td>
    <td class="respuesta" id="provincia"></td>
    <td>CUIE:</td>
    <td class="respuesta" id="cuie"></td>
  </tr>
</table>
</div>
</body>
</html>
