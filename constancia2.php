<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.66.0-2013.10.09/jquery.blockUI.min.js"></script>

<link rel="shortcut icon" href="http://programasumar.com.ar/favicon.ico">
<title>Cobertura Universal de Salud - SUMAR - Ministerio de Salud de la Naci&oacute;n</title>
<style type="text/css">

.Fecha {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #999999;
}
.Area {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #009ADF;
}
.Aviso {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 9px;
  color: #990000;
}
a:link {
  color: #FFFFFF;
  text-decoration: none;
}
a:visited {
  color: #FFFFFF;
  text-decoration: none;
}
a:hover {
  text-decoration: none;
  color: #333333;
}
a:active {
  text-decoration: none;
  color: #FFFFFF;
}
.Calendario{
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 12px;
  color: #333333;
}
.Calendario2{
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 12px;
  font-weight:bold;
  color: #333333;
}
.GrisClaro{
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 12px;
  color: #999999;
}
.Grande {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 16px;
  color: #666666;
}
.Grande2 {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #009ADF;
  font-weight: bold;
}
.Grande3 {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #666666;
  font-weight: bold;
}
.Cargo {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 12px;
  color: #009ADF;
  font-weight: bold;
}
.Cargo2 {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 12px;
  color: #FFFFFF;
  font-weight: bold;
}
.Texto {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #333333;
}
.TituloBlanco {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  font-weight: bold;
  color: #FFFFFF;
}
.TextoBlanco {
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 10px;
  font-weight: bold;
  color: #FFFFFF;
}

#datos_a_imprimir{
  margin-left: 340px;
  margin-top: 130px;
  border-color: lightblue;
  border-style: solid;
  width: 750px;
}

#tabla_datos_a_imprimir
{
  position: relative;
  text-align: left;
  border-style: solid;
  width: 700px;
  margin: 25px;
  border: 1px solid;
  border-color: #B0B0B0;
  border-collapse: collapse;
}

#tabla_datos_a_imprimir td
{
  border-top: 1px solid;
  border-color: #B0B0B0;
  border-collapse: collapse;
  padding: 5px;
  width: 25%;
  font-family: Geneva, Arial, Helvetica, sans-serif;
  font-size: 14px;
  color: #333333;
}

#titulos{
  position: relative;
  width: 700px;
  margin: 25px;
}

body > div{
  margin-top: 10px;
  margin-left: 47%;
}

#mensaje_no_activo{
  display:none;
  position: relative;
  text-align: center;
  margin-top:35px;
}
.respuesta{
  font-weight: bold;
}
.Estilo1 {font-family: Geneva, Arial, Helvetica, sans-serif; font-size: 16px; color: #009ADF; font-weight: bold; }

</style>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

</script>

</head>

<body>
<script type="text/javascript">
  $(document).ready(function(event) {

      console.log("<?php echo $_POST['benef_documento']?>");
      var documento = "<?php echo $_POST['benef_documento']?>";

      $.blockUI({
        message: '<h1 style="text-align:center; vertical-align:top;"><img style="margin-right:25px;" src="imagenes/big-roller-loader.gif" />Cargando...</h1>'

    });

      var datos = "doc="+documento;


      $.ajax({
        url: 'busquedaBenef.php',
        type: 'POST',
        dataType: 'json',
        data: datos,
        success: function(data){
           console.log(data);

           if(data == null || data == [] || data == ""){
            $("#mensaje_no_encontrado").show();
           }
           else if(data.activo == "S"){
            $("#tabla_beneficiario").show();
           }
           else if(data.activo == "N"){
            $("#mensaje_no_activo").show();
            $("#motivo").text(data.mensaje);
           }

           $("#apellidos").text(data.apellido);
           $("#nombres").text(data.nombre);
           $("#tipo_doc").text(data.tipo_documento);
           $("#doc").text(data.numero_documento);
           $("#clave_benef").text(data.clave_beneficiario);
           $("#fecha_ins").text(data.fecha_inscripcion);
           $("#provincia").text(data.provincia);
           $("#cuie").text(data.cuie);
        }
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
         $.unblockUI();
        console.log("complete");
      });

      $("#imprimir").on('click',function(event) {
          event.preventDefault();

          var datos_a_imprimir = "apellidos="+$("#apellidos").text()+"&nombres="+$("#nombres").text()+"&tipo_doc="+$("#tipo_doc").text()+"&doc="+$("#doc").text()+"&clave_benef="+$("#clave_benef").text()+"&fecha_ins="+$("#fecha_ins").text()+"&provincia="+$("#provincia").text()+"&cuie="+$("#cuie").text();

          console.log(datos_a_imprimir);

          window.open("APDF.php?"+datos_a_imprimir);
      });

  });
</script>


<table id="contenido_a_imprimir" style="margin-left:auto; margin-right:auto;" width="100" border="0" align="center" cellpadding="0" cellspacing="2">
  <tr>
    <td height="528" align="center"><table width="1000" height="521" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td valign="bottom" align="center"><img src="imagenes/header-sumar.jpg" width="80%" height="168" /></td>
      </tr>     
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1%" background="imagenes/barra.gif" bgcolor="#4396DE"><img src="imagenes/barra.gif" width="2" height="33" /></td>
            <td width="99%" align="right" background="imagenes/barra.gif" bgcolor="#4396DE"> <div id="volver" style="display:inline-block; margin-right:30px;"> <img src="imagenes/ic_04.gif" width="25" height="25" border="0" bgcolor="#4396DE" style="position:relative; vertical-align:middle;"> <a href="index.html" style="vertical-align:sub;"> <span><b>Volver</b></span> </a></div></td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td height="343" align="center" valign="top" bgcolor="#FFFFFF">

        <table style="display:none;" id="tabla_beneficiario" width="983" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" valign="top"><table width="100" border="0" cellspacing="20" cellpadding="0">
              <tr>
                <td><table width="100" border="0" cellspacing="5" cellpadding="0">
                <tr>
                    <td><a id="imprimir" href="#"><img src="imagenes/impr.gif" alt="&lt;Imprimir&gt;" width="43" height="43" border="0" /></a></td>
                    <td valign="middle" class="Texto">Imprimir</td>
                  </tr>
                </table>
                </td>
              </tr>
            </table>
              <table id="constancia2" width="100" border="1" cellpadding="10" cellspacing="0" bordercolor="#00AFF0">
              <tr>
                <td><table id="constancia2_a_imprimir" width="701" border="0" cellpadding="7" cellspacing="5">
                  <tr>
                    <td width="358" valign="middle" class="Estilo1"><div align="left">CONSTANCIA DE INSCRIPCI&Oacute;N<BR />
                      </div></td>
                    <td width="350" align="right"><a style="margin-right:20px;" href="http://www.msal.gov.ar/sumar/"><!-- <img src="imagenes/logo_sumar.png" alt="Programa SUMAR" width="71" height="71" border="0" /></a><a href="http://www.msal.gov.ar/"> --><img src="imagenes/logo_msal.png" alt="Ministerio de Salud de la Nación" width="216" height="49" border="0" /></a></td>
                  </tr>
                  <tr id="datos_beneficiario">
                    <td colspan="2" bgcolor="#F0F0F0"><table width="650" border="0" align="center" cellpadding="4" cellspacing="4">
                          <tr>
                            <td width="61" class="Texto"><div align="left">Apellidos:</div></td>
                            <td width="561" bgcolor="#FFFFFF" class="Texto"><div id="apellidos" align="left"></div></td>
                          </tr>
                          <tr>
                            <td class="Texto"><div align="left">Nombres</div></td>
                            <td bgcolor="#FFFFFF" class="Texto"><div id="nombres" align="left"></div></td>
                          </tr>
                                          </table>
                      <table width="650" border="0" align="center" cellpadding="4" cellspacing="4">
                            <tr>
                              <td width="126" nowrap="nowrap" class="Texto"><div align="left">Tipo de documento:</div></td>
                              <td width="141" bgcolor="#FFFFFF" class="Texto"><div id="tipo_doc" align="left"></div></td>
                              <td width="147" nowrap="nowrap" class="Texto"><div align="left">Numero de documento:</div></td>
                              <td width="184" bgcolor="#FFFFFF" class="Texto"><div id="doc" align="left"></div></td>
                            </tr>
                                              </table>
                      <table width="650" border="0" align="center" cellpadding="4" cellspacing="4">
                            <tr>
                              <td width="137" nowrap="nowrap" class="Texto"><div align="left">Clave de beneficiario:</div></td>
                              <td width="202" bgcolor="#FFFFFF" class="Texto"><div id="clave_benef" align="left"></div></td>
                              <td width="135" nowrap="nowrap" class="Texto"><div align="left">Fecha de inscripción:</div></td>
                              <td width="124" bgcolor="#FFFFFF" class="Texto"><div id="fecha_ins" align="left"></div></td>
                            </tr>
                                              </table>                      <table width="650" border="0" align="center" cellpadding="4" cellspacing="4">
                            <tr>
                              <td width="62" nowrap="nowrap" class="Texto"><div align="left">Provincia:</div></td>
                              <td width="411" bgcolor="#FFFFFF" class="Texto"><div id="provincia" align="left"></div></td>
                              <td width="39" nowrap="nowrap" class="Texto"><div align="left">Cuie:</div></td>
                              <td width="86" bgcolor="#FFFFFF" class="Texto"><div id="cuie" align="left"></div></td>
                            </tr>
                                            </table></td>
                    </tr>

                </table></td>
              </tr>
            </table>
              <p>&nbsp;</p></td>
            </tr>
        </table>

        <table style="display:none; margin-top:35px;" id="mensaje_no_encontrado" width="31%" border="0" cellspacing="20">
                <tr>
                  <td align="center" nowrap="nowrap" class="cumple2"><img src="imagenes/atencion.gif" width="67" height="67" /></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Grande4"><div align="center" class="Estilo1">No se encontró el registro.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Puede deberse a que realizó la inscripción recientemente<br />o que no se encuentra inscripto.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Para inscribise en Cobertura Universal de Salud - SUMAR deber&aacute;<br />
                    concurrir al centro de salud u hospital p&uacute;blico<br />
                    m&aacute;s cercano a su domicilio con su DNI. </div></td>
                </tr>
              </table>
        <table id="mensaje_no_activo" border="0" cellspacing="20">
                <tr>
                  <td align="center" nowrap="nowrap" class="cumple2"><img src="imagenes/atencion.gif" width="67" height="67"/></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Grande4"><div align="center" class="Estilo1">La inscripción no se encuentra activa.</div></td>
                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div id="motivo" align="center">Puede deberse a que posee obra social o prepaga.<br />O no se encuentra dentro de la poblaci&oacute;n objetivo de Cobertura Universal de Salud - SUMAR.</div></td>

                </tr>
                <tr>
                  <td align="center" nowrap="nowrap" class="Calendario"><div align="center">Para mas informaci&oacute;n puede comunicarse de manera<br />
                    gratuita llamando desde un tel&eacute;fono fijo al 0800.222.7100.</div></td>
                </tr>
              </table>
              </td>
        </tr>
      <tr>
        <td colspan="3" align="right" valign="top"><img src="imagenes/somb4.png" width="100%" height="11" /></td>
        </tr>
    </table>
    </td>
  </tr>
</table>

<div style="display:none;">
<script id="_wauw4d">
var _wau = _wau || [];
_wau.push(["colored", "8yddx313aeew", "w4d", "004ab2d7ffe7"]);
(function() {var s=document.createElement("script"); s.async=true;
s.src="http://widgets.amung.us/colored.js";
document.getElementsByTagName("head")[0].appendChild(s);
})();
</script>

<!-- http://whos.amung.us/stats/8yddx313aeew/ -->
</div>
</body>
</html>